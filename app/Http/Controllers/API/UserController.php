<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use Illuminate\Support\Facades\Auth; 
use App\User; 
use Validator;

class UserController extends Controller 

{

    public function __construct(User $user){
        $this->user = $user;
    }
    
    /** 
     * login api 
     * 
     * @return json response
     */ 
    public function login(){ 

        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
            $user = Auth::user(); 
            $success['token'] =  $user->createToken('MyApp')->accessToken; 
            return response()->json(['success' => $success], 200); 
        } 
        else{ 
            return response()->json(['error'=>'Unauthorised'], 401); 
        } 
    }

    /** 
     * Register api 
     * 
     * @return json response
     */ 
    public function register(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'email' => 'required|email|unique:users,email,'.$id, 
            'password' => 'required', 
            'c_password' => 'required|same:password', 
        ]);
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
        $input = $request->all(); 
        $input['password'] = bcrypt($input['password']); 
        $user = User::create($input); 
        $success['token'] =  $user->createToken('MyApp')->accessToken; 
        $success['name'] =  $user->name;
        return response()->json(['success'=>$success], 200); 
    }
    
    /** 
     * details api 
     * 
     * @return user data as json response
     */ 
    public function details() 
    { 
        
        if(Auth::check()) {
            $user = Auth::user(); 
            return response()->json(['success' => $user], 200); 
        }        
        return response()->json(['success' => true, 'message' => 'Logout successfully'], 200);
    } 

    /**
     * Handle Update user data
     * 
     * @param $request
     * 
     * @return json response
     */
    public function updatedetails(Request $request){
        
        $user_id = Auth::user()->id;

        // dd($user_id);
        $dataarray = array(
            'name' => 'required',
        );

        $input = $request->all(); 
        if(isset($request->password)){
            $input['password'] = bcrypt($input['password']); 
            $dataarray['password'] = 'required';
            $dataarray['c_password'] = 'required|same:password';
        }
        //Validation
        $validator = Validator::make($request->all(), $dataarray);

        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
        $user = $this->user::find($user_id);
        $user->update($request->all());
        
        return response()->json(['success'=>$user, ], 200); 

    }
     
    /**
     * Handle a user logout .
     *
     * @return json response
     */
    public function logout(Request $request)
    { 
        if(Auth::check()) {
            $accessToken = Auth::user()->token();
            $token= $request->user()->tokens->find($accessToken);
            $token->revoke();
        }        
        return response()->json(['success' => true, 'message' => 'Logout successfully'], 200); 
    }
    
    /**
     * Handle a user logout .
     *
     * @return json response
     */
    public function unauth(Request $request)
    { 
        return response()->json(['error'=>'Unauthorised Access'], 401); 
    }
    

}